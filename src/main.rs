use structopt::StructOpt;
use pwdrs_core::generator::{PwdGenerator, CharCollection};
use std::io;
use std::path::Path;
use std::fs::File;
use clipboard::ClipboardContext;
use clipboard::ClipboardProvider;
use std::io::prelude::*;

fn main() {
    println!(
        "{} v.{}\n",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );

    let settings = PwdSettings::new();
    Generator::run(&settings);
}

#[allow(clippy::struct_excessive_bools)]
#[derive(Default)]
#[derive(StructOpt)]
#[structopt(name = "pwd", about = "a simple password generator")]
pub struct PwdSettings {
    #[structopt(
        short = "d",
        long = "has-no-digits",
        help = "Removes digits to the generation process."
    )]
    pub has_no_digits: bool,
    #[structopt(
        short = "u",
        long = "has-no-uppercase",
        help = "Removes uppercase chars to the generation process."
    )]
    pub has_no_uppercase: bool,
    #[structopt(
        short = "s",
        long = "has-no-special-chars",
        help = "Removes special chars to the generation process."
    )]
    pub has_no_special_chars: bool,
    #[structopt(
        short = "C",
        long = "copy-to-clipboard",
        help = "Copies the password to the clipboard."
    )]
    pub copy_to_clipboard: bool,
    #[structopt(
        short = "f",
        long = "copy-to-file",
        help = "Copies the password to a file."
    )]
    pub copy_to_file: bool,
    #[structopt(
        short = "n",
        long = "no-pwd-display",
        help = "Disables display of pwd."
    )]
    pub no_pwd_display: bool,
    #[structopt(
        short = "l",
        long = "length",
        default_value = "16",
        help = "Defines the number of chars in the password"
    )]
    pub length: u32,
    #[structopt(
        short = "c",
        long = "chars-file-location",
        default_value = "./chars.json",
        help = "Defines the location of the chars file"
    )]
    pub chars_file_location: String,
}

impl PwdSettings {
    #[must_use]
    pub fn new() -> PwdSettings {
        PwdSettings::from_args()
    }
}

pub struct Generator {}

impl Generator {
    pub fn run(settings: &PwdSettings) {
        Self::display_settings(&settings);

        let char_collection = Self::load_char_collection(&settings.chars_file_location);

        let generator = PwdGenerator::new_with(
            settings.has_no_digits,
            settings.has_no_uppercase,
            settings.has_no_special_chars,
            settings.length,
        );
        let pwd = generator.generate(&char_collection);

        if !settings.no_pwd_display {
            println!("Your password: {}\n", pwd);
        }
        if settings.copy_to_clipboard {
            let mut ctx: ClipboardContext = match ClipboardProvider::new() {
                Ok(c) => c,
                Err(err) => {
                    println!("Error at coping to clipboard: {}", err);
                    println!("Password not copied to clipboard!");

                    return;
                }
            };

            ctx.set_contents(pwd.clone())
                .expect("Copied to clipboard faild!");

            println!("Copied password to clipboard.\n");
        }
        if settings.copy_to_file {
            Self::write_pwd_to_file(&pwd);
        }
    }
}

impl Generator {
    fn load_char_collection(chars_file_location: &str) -> CharCollection {
        if Path::new(chars_file_location).exists() {
            let mut file = File::open(chars_file_location).unwrap();
            let mut contents = String::new();

            file.read_to_string(&mut contents).unwrap();

            match CharCollection::from_json(&contents) {
                Ok(c) => c,
                Err(err) => {
                    println!("{}", err);
                    println!("Could not read json!");
                    println!("Now working with default settings.\n");

                    let char_collection = CharCollection::default();
                    Self::write_char_file(chars_file_location, &char_collection);

                    char_collection
                }
            }
        } else {
            println!("File \"{}\" doesn't exist!", chars_file_location);
            println!("Now working with default settings.\n");

            let char_collection = CharCollection::default();
            Self::write_char_file(chars_file_location, &char_collection);

            char_collection
        }
    }

    fn write_char_file(char_file_location: &str, char_collection: &CharCollection) {
        let mut input = String::new();

        while !input.contains('y')
            && !input.contains('Y')
            && !input.contains('n')
            && !input.contains('N')
        {
            println!("Do you want to create a new file at the location? (y/n)");

            io::stdin()
                .read_line(&mut input)
                .expect("Failed to read line");
        }

        if input.contains('y') || input.contains('Y') {
            let json = char_collection.to_json();

            let mut file = File::create(char_file_location).unwrap();
            file.write_all(json.as_bytes()).unwrap();

            println!(
                "A new chars file has been created under \"{}\".",
                char_file_location
            );
        }
        println!();
    }

    fn display_settings(settings: &PwdSettings) {
        println!("Your settings: ");
        println!("Has digits: {}", !settings.has_no_digits);
        println!("Has uppercase: {}", !settings.has_no_uppercase);
        println!("Has special chars: {}", !settings.has_no_special_chars);
        println!("Copy to clipboard: {}", !settings.copy_to_clipboard);
        println!("Copy to file: {}", !settings.copy_to_file);
        println!("Password display enabled: {}", !settings.no_pwd_display);
        println!("Password length: {}\n", settings.length);
        println!("Chars file: {}\n", settings.chars_file_location);
    }

    fn write_pwd_to_file(pwd: &str) {
        let mut input = String::new();

        println!("Please enter a filename. Existing files woll be overwritten!");

        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");

        let mut file = File::create(&input).unwrap();
        file.write_all(pwd.as_bytes()).unwrap();

        println!("The password has been saved under \"{}\".\n", input);
    }
}